Ametros Dive Computer
=====================

Copyright (C) 2010 Geoff Johnstone. All rights reserved.

Do not use this for diving. If you use this for diving and you kill yourself
or get decompression sickness then it's your own fault. If you choose to
ignore this advice then please don't rely on the numbers that this program
generates. Use other tools. Check its output. Get someone else to do the dive
first, and only dive yourself if they survive, etc.

This program is provided as is, WITHOUT WARRANTIES OF ANY KIND, including
fitness for any purpose whatsoever.

YOU USE THIS AT YOUR OWN RISK. If you kill yourself, don't come round to my
place expecting any sympathy / apology / compensation.

If you don't know me and trust me with your life then you're stupid.
If you do know me then you'll know not to trust me with your life.

If you're still interested and prepared to take responsibility for your own
actions, rather than expecting someone else to, then please read on.


Introduction
------------
The Ametros Dive Computer reads dive plans in a variety of formats (including
DDPlan and APD Projection HTML output), computes *ascent* gas volumes and
outputs the result in a variety of formats. Surviving your descent to the
bottom and whatever you get up to down there is your own problem.


Usage
-----
$ java -jar [options] plan...

Options:

 -f, --format : output format: html, xhtml, xml or xls.
 -o, --output : output filename.

If you omit -o / --output then it outputs to standard output.
If you omit -f / --format but include -o / --output then it guesses the format.


Licence
-------
GNU General Public Licence v3, assuming that that's compatible with the
various third-party libraries that I've used. If not, speak to a lawyer
and suggest something else to me.


Third-Party Libraries
---------------------
The Ametros Dive Computer uses the following third-party libraries:

Apache Velocity for [X]HTML output:

  Apache Velocity

  Copyright (C) 2000-2007 The Apache Software Foundation

  This product includes software developed at
  The Apache Software Foundation (http://www.apache.org/).

Apache POI for XLS handling:

  Apache POI
  Copyright 2009 The Apache Software Foundation

  This product includes software developed by
  The Apache Software Foundation (http://www.apache.org/).

  This product contains the DOM4J library (http://www.dom4j.org).
  Copyright 2001-2005 (C) MetaStuff, Ltd. All Rights Reserved.

  This product contains parts that were originally based on software from BEA.
  Copyright (c) 2000-2003, BEA Systems, <http://www.bea.com/>.

  This product contains W3C XML Schema documents. Copyright 2001-2003 (c)
  World Wide Web Consortium (Massachusetts Institute of Technology, European
  Research Consortium for Informatics and Mathematics, Keio University)

  This product contains the Piccolo XML Parser for Java
  (http://piccolo.sourceforge.net/). Copyright 2002 Yuval Oren.

  This product contains the chunks_parse_cmds.tbl file from the vsdump program.
  Copyright (C) 2006-2007 Valek Filippov (frob@df.ru)

JOptSimple for command line parsing:

  Copyright (c) 2009 Paul R. Holser, Jr.

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

