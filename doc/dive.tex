\hsize=17cm\vsize=24.7cm\hoffset=-.54cm\voffset=-5pt
\parindent=0pt\parskip=12pt
\baselineskip=14pt
\font\title=cmb10 at14pt
\font\sec=cmssbx10 at12pt
\font\ssec=cmssbx10 at11pt
\font\sssec=cmssbx10 at10pt
\newcount\seccnt\newcount\sseccnt\newcount\ssseccnt
\seccnt=0\sseccnt=0\ssseccnt=0
\def\section#1{\global\sseccnt=0\global\ssseccnt=0\global\advance\seccnt by1%
   \vskip6pt{\sec\indent\llap{\the\seccnt~~}#1}\par\vskip-4pt}
\def\Section#1{\global\sseccnt=0\global\ssseccnt=0\vskip6pt{\sec{}#1}\par\vskip-4pt}
\def\ssection#1{\global\ssseccnt=0\global\advance\sseccnt by1%
   {\ssec\indent\llap{\the\seccnt.\the\sseccnt~~}#1}\vskip-6pt}
\def\sssection#1{\global\advance\ssseccnt by1%
   {\sssec\indent\llap{\the\seccnt.\the\sseccnt.\the\ssseccnt~~}#1}\vskip-9pt}
\def\SSSection#1{\global\advance\ssseccnt by1%
   {\sssec{}#1}\vskip-9pt}

\def\atm{\hbox{atm}}
\def\Pa{\hbox{Pa}}
\def\defs{\mathbin{\dot=}}
\def\sac{\hbox{SAC}}
\def\lpm{\hbox{$\ell$/min}}
\def\rmv{\hbox{RMV}}

\newcount\fn\fn=0
\def\fnote{\advance\fn by1\footnote{$^{\hbox{\sevenrm\the\fn}}$}}

\section{Pressure}
The pressure $P_m$ in atmospheres\fnote{$1~\atm \defs 101325~\Pa$} exerted by
one metre of a substance is defined in terms of acceleration due to gravity
$g$ and density $\rho$ as:
$$P_m = {g\rho\over101325}$$
Sea water density varies with salt concentration and temperature. A common
approximation for diving is $1025~\hbox{kg/m$^3$}$, which,
with $g = g_0 \defs 9.80665~\hbox{ms$^{-2}$}$, gives:
$$P_m' = {9.80665~\cdotp~1025\over101325} \approx {1\over10}$$
The pressure $P(d)$ exerted on the diver at a depth $d$~m is atmospheric
pressure plus water pressure:
$$P(d) = 1 + dP_m \hskip3cm P'(d) \approx 1 + {d\over10}$$
Some references call this ``atmospheres absolute'' or ATA.

\section{Respiration}
We shall define two constants for the volume of air breathed per unit time at
the surface\fnote{Other derivations use pressure rather than volume.}:
$$\eqalign{
  \hbox{Resting surface air consumption}~&\sac_r \defs 20~\lpm\cr
  \hbox{Active surface air consumption}~&\sac_a \defs 30~\lpm
}$$
The respiratory minute volume $\rmv$ is the volume of air breathed per unit
time at depth $d$, in litres per minute:
$$\eqalign{
  \rmv_r(d) &= P(d)~\sac_r \cr
  \rmv_a(d) &= P(d)~\sac_a \cr
}$$

\section{Ascent from Depth}
An ascent from depth comprises ascent phases separated by decompression stops.
The diver may use several gases during the ascent depending on the depth,
which we shall represent with the function $g_a(g, d)$, defined to be 1 if gas
$g$ is in use at depth $d$, else 0. We want the volume $G(g)$ of each gas
required for the ascent. This can be treated as the sum of the volume required
for the ascent phases and the volume required for the stops:
$$G(g) = G_A(g) + G_S(g)$$

\ssection{Ascent}
For a given ascent from a depth $d$, and disregarding stops, define the
function $D(t)$ which gives the depth at time $t$. The gas volume required for
the ascent is:
$$G_A = \int_t \rmv_a(D(t))~dt$$
This decomposes into the volumes for the various gases thus:
$$G_A (g) = \int_t g_a(g, D(t))~\rmv_a(D(t))~dt$$

\ssection{Stops} We shall assume that the diver makes $N$ stops en
route\fnote{Deriving $N$, $D_i$ and $T_i$ is beyond the scope of this
document.}. The $i$th stop occurs at depth $D_i$ and has a duration $T_i$
minutes. The total gas volume required for the stops is:
$$G_S = \sum_{1\leq i\leq N} \rmv_r(D_i)~T_i$$
which decomposes thus:
$$G_S (g) = \sum_{1\leq i\leq N} g_a (g, D_i)~\rmv_r(D_i)~T_i$$

\ssection{Simplifying Assumptions}
A common assumption is a constant rate of ascent, $V_a$, during the ascent
phases. This gives $D(t) = d - V_at$~:
$$\eqalign{
  G_A' &= \int_{t_0}^{t_1} \rmv_a\,(d - V_a\,t)~dt \cr
       &= \int_{t_0}^{t_1} P\,(d - V_a\,t)~\sac_a~dt \cr
       &= \sac_a \int_{t_0}^{t_1} 1 + (d - V_a\,t)P_m~dt \cr
       &= \sac_a \left[t + dtP_m - {V_a\,t^2\,P_m\over 2} \right]_{t_0}^{t_1}\cr
       &= \sac_a \Bigg(\hskip-3pt
            \left(t_1 + dt_1P_m - {V_a\,t_1^2\,P_m\over2} \right) -
            \left(t_0 + dt_0P_m - {V_a\,t_0^2\,P_m\over2} \right)%
          \hskip-3pt\Bigg)\cr
       &= \sac_a~(t_1 - t_0) \left(
            1 + dP_m - {(t_1 + t_0)\,V_a\,P_m\over2}
          \right)\cr
}$$
To compute the gas volume for the whole ascent take $t_0=0$ and
$t_1=d / V_a$, giving:
$$G_A'' = \sac_a~{d\over V_a} \left(1 + {dP_m\over2}\right)$$
For example, ascending at $V_a = 8~\hbox{m/minute}$ from
$d =35\hbox{m}$ gives $G_A'' = 360~\ell$.  Ascending at the same rate from $d
=12\hbox{m}$ gives $G_A'' = 72~\ell$.

This is analytically identical to the result under the (notionally stronger)
assumption that we consume gas at a constant rate throughout the ascent, and
take that rate as that consumed at the depth $d' = d/2$. This is because
$\rmv_a(d)$ is linear in $d$.

\bye

