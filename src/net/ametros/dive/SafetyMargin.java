/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;


/** Allows modification of DiveComputer calculations at various stages. */
public interface SafetyMargin
{
  /** Modifies the volume of gas consumed during decompression stops on a dive.
   *  @param dive the dive.
   *  @param gas the gas.
   *  @param original the theoretical required volume of gas (litres).
   *  @return the required volume of gas (litres).
   */
  public double getStopPhaseVolume (Dive dive, Gas gas, double original);


  /** Modifies the volume of gas consumed whilst ascending from a dive.
   *  @param dive the dive.
   *  @param gas the gas.
   *  @param original the theoretical required volume of gas (litres).
   *  @return the required volume of gas (litres).
   */
  public double getAscentPhaseVolume (Dive dive, Gas gas, double original);


  /** Modifies the volume of gas consumed during the complete ascent, i.e.
   *  the total consumed during the ascent and during any decompression stops.
   *  @param dive the dive.
   *  @param gas the gas.
   *  @param original the theoretical required volume of gas (litres).
   *  @return the required volume of gas (litres).
   */
  public double getAscentTotalVolume (Dive dive, Gas gas, double original);


  /** Modifies the volume of gas consumed at the bottom.
   *  @param dive the dive.
   *  @param original the theoretical required volume of gas (litres).
   *  @return the required volume of gas (litres).
   */
  public double getBottomVolume (Dive dive, double original);


  /** Modifies the depth interval within which a given gas is used whilst
   *  ascending from a dive (excluding stops).
   *  @param dive the dive.
   *  @param gas the gas.
   *  @param original the planned depth interval.
   *  @return the depth interval to use for gas volume calculations.
   */
  public Interval getInterval (Dive dive, Gas gas, Interval original);
}

