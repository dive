/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;


/** An integer range. */
public class Interval
{
  private final int i0;
  private final int i1;


  public Interval (Interval i)
  {
    this (i.getLower(), i.getHigher());
  }


  public Interval (int i0, int i1)
  {
    this.i0 = (i0 < i1) ? i0 : i1;
    this.i1 = (i0 < i1) ? i1 : i0;
  }


  public int getLower()
  {
    return i0;
  }


  public int getHigher()
  {
    return i1;
  }


  public int getRange()
  {
    return getHigher() - getLower();
  }


  @Override
  public String toString()
  {
    return "[" + getLower() + "," + getHigher() + "]";
  }
}

