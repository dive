/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;

import net.ametros.dive.data.Gas;
import net.ametros.dive.data.Stop;


public final class ComputedStop
{
  private final int depth;
  private final int duration;
  private final Gas gas;
  private final double gasVolume;


  public ComputedStop (Stop stop, double gasVolume)
  {
    this.depth = stop.getDepth();
    this.duration = stop.getDuration();
    this.gas = stop.getGas();
    this.gasVolume = gasVolume;
  }


  public int getDepth()
  {
    return depth;
  }


  public int getDuration()
  {
    return duration;
  }


  public Gas getGas()
  {
    return gas;
  }


  public double getGasVolume()
  {
    return gasVolume;
  }
}

