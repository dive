/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;
import net.ametros.dive.data.Stop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public final class ComputedDive
{
  private final Dive dive;
  private final int depth;
  private final int duration;
  private final Gas bottomGas;
  private final List<ComputedStop> stops;
  private final Map<Gas, Double> ascentVolumes =
    new EnumMap<Gas, Double> (Gas.class);


  public ComputedDive (Dive dive, double V_a, DiveComputer dc)
  {
    this.dive = dive;
    this.depth = dive.getDepth();
    this.duration = dive.getDuration();
    this.bottomGas = dive.getGas();

    // Compute a set of all required gases and a list of ComputedStops.
    final Set<Gas> gases = EnumSet.of (bottomGas);
    final List<Stop> stops = dive.getAscent().getStops();
    this.stops = new ArrayList<ComputedStop> (stops.size());

    for (Stop stop: stops)
    {
      this.stops.add (new ComputedStop (stop, dc.getStopGasVolume (stop)));
      gases.add (stop.getGas());
    }

    // Gas volumes required during the ascent.
    for (Gas gas: gases)
      ascentVolumes.put (gas, dc.getAscentTotalVolume (V_a, gas));
  }


  public Dive getDive()
  {
    return dive;
  }


  public int getDepth()
  {
    return depth;
  }


  public int getDuration()
  {
    return duration;
  }


  public Gas getGas()
  {
    return bottomGas;
  }


  public List<ComputedStop> getStops()
  {
    return Collections.unmodifiableList (stops);
  }


  public ComputedStop getStop (ComputedStop stopKey)
  {
    return getStop (stopKey.getGas(), stopKey.getDepth());
  }


  public ComputedStop getStop (Gas gas, int depth)
  {
    for (ComputedStop stop: stops)
      if ((stop.getDepth() == depth) && stop.getGas().equals (gas))
        return stop;

    return null;
  }


  public boolean hasStop (ComputedStop stopKey)
  {
    return hasStop (stopKey.getGas(), stopKey.getDepth());
  }


  public boolean hasStop (Gas gas, int depth)
  {
    for (ComputedStop stop: stops)
      if ((stop.getDepth() == depth) && stop.getGas().equals (gas))
        return true;

    return false;
  }


  public int getTripTime()
  {
    int ret = getDuration();

    for (ComputedStop stop: stops)
      ret += stop.getDuration();

    return ret;
  }


  public Set<Gas> getGases()
  {
    return ascentVolumes.keySet();
  }


  public double getAscentVolume (Gas gas)
  {
    return ascentVolumes.containsKey (gas) ? ascentVolumes.get (gas) : 0;
  }
}

