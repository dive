/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.writer;

import net.ametros.dive.ComputedDive;
import net.ametros.dive.ComputedStop;
import net.ametros.dive.data.Gas;

import java.io.OutputStream;
import java.util.EnumMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;


/** Writes Excel dive tables. */
public class XLSWriter extends AbstractWriter
{
  protected HSSFWorkbook createWorkbook() throws Exception
  {
    final HSSFWorkbook wb = new HSSFWorkbook();
    createStyles (wb);

    final HSSFSheet sheet = wb.createSheet ("Output");
    sheet.createFreezePane (2, 0);

    int row = 0;

    for (int depth: dives.keySet())
    {
      final int firstRow = row;

      // Depth row.
      sheet.createRow (row++).createCell (0).setCellValue ("" + depth + 'm');

      // Title row.
      final int titleRow = row++;
      final HSSFRow title = sheet.createRow (titleRow);
      int col = 0;
      final int firstCol = col;

      title.createCell (col++).setCellValue ("Gas");
      title.createCell (col++).setCellValue ("D\\RT");

      for (ComputedDive d: dives.get (depth))
      {
        sheet.setColumnWidth (col, 256);
        title.createCell (col + 1).setCellValue (d.getDuration());
        sheet.addMergedRegion (new CellRangeAddress (titleRow, titleRow,
                                                     col + 1, col + 2));
        col += 3;
      }

      final int lastCol = col - 1;

      // Stops.
      for (ComputedStop key: getStopKeys (depth))
      {
        final int stopDepth = key.getDepth();
        final Gas gas = key.getGas();
        final HSSFRow r = sheet.createRow (row++);

        col = 0;
        r.createCell (col++).setCellValue (format (gas));
        r.createCell (col++).setCellValue (stopDepth);

        for (ComputedDive d: dives.get (depth))
        {
          final ComputedStop stop = d.getStop (gas, stopDepth);
          if (null != stop)
          {
            final double litres = stop.getGasVolume();
            r.createCell (col + 1).setCellValue (stop.getDuration());
            r.createCell (col + 2).setCellValue ((long)Math.ceil (litres));
          }

          col += 3;
        }
      }

      // Totals.
      final int firstTitleRow = row;
      final HSSFRow timeRow = sheet.createRow (row++);
      final Map<Gas, HSSFRow> gasRows = new EnumMap<Gas, HSSFRow>(Gas.class);
      col = 2;

      for (ComputedDive d: dives.get (depth))
      {
        timeRow.createCell (col + 1).setCellValue ("Run time");
        timeRow.createCell (col + 2).setCellValue (d.getTripTime());

        for (Gas gas: d.getGases())
        {
          if (!gasRows.containsKey (gas))
            gasRows.put (gas, sheet.createRow (row++));

          final HSSFRow r = gasRows.get (gas);
          final double litres = d.getAscentVolume (gas);
          r.createCell (col + 1).setCellValue (format (gas));
          r.createCell (col + 2).setCellValue ((long)Math.ceil (litres));
        }

        col += 3;
      }

      final int lastRow = row - 1;
      tartUpDive (wb, sheet, firstRow, lastRow, firstCol, lastCol,
                  firstTitleRow);

      // Empty row between depths.
      ++row;
    }

    clearStyles();
    return wb;
  }


  private HSSFCellStyle styleTL = null;
  private HSSFCellStyle styleT = null;
  private HSSFCellStyle styleTR = null;
  private HSSFCellStyle styleL = null;
  private HSSFCellStyle styleR = null;
  private HSSFCellStyle styleBL = null;
  private HSSFCellStyle styleB = null;
  private HSSFCellStyle styleBR = null;
  private HSSFCellStyle styleDuration = null;

  protected void createStyles (HSSFWorkbook wb)
  {
    styleTL = createBorder (wb, true, true, false, false);
    styleT = createBorder (wb, true, false, false, false);
    styleTR = createBorder (wb, true, false, false, true);
    styleL = createBorder (wb, false, true, false, false);
    styleR = createBorder (wb, false, false, false, true);
    styleBL = createBorder (wb, false, true, true, false);
    styleB = createBorder (wb, false, false, true, false);
    styleBR = createBorder (wb, false, false, true, true);

    final Font bold = wb.createFont();
    bold.setBoldweight (Font.BOLDWEIGHT_BOLD);
    styleDuration = wb.createCellStyle();
    styleDuration.setAlignment (CellStyle.ALIGN_CENTER);
    styleDuration.setFont (bold);
  }


  protected void clearStyles()
  {
    styleTL = null;
    styleT = null;
    styleTR = null;
    styleL = null;
    styleR = null;
    styleBL = null;
    styleB = null;
    styleBR = null;
    styleDuration = null;
  }


  protected void tartUpDive (HSSFWorkbook wb, HSSFSheet sheet,
                             int firstRow, int lastRow,
                             int firstCol, int lastCol,
                             int firstTitleRow) throws Exception
  {
    setCellStyle (sheet, firstRow, firstCol, styleTL);
    setCellStyle (sheet, firstRow, lastCol, styleTR);
    for (int i = firstCol + 1; i < lastCol; ++i)
      setCellStyle (sheet, firstRow, i, styleT);

    setCellStyle (sheet, lastRow, firstCol, styleBL);
    setCellStyle (sheet, lastRow, lastCol, styleBR);
    for (int i = firstCol + 1; i < lastCol; ++i)
      setCellStyle (sheet, lastRow, i, styleB);

    for (int i = firstRow + 1; i < lastRow; ++i)
    {
      setCellStyle (sheet, i, firstCol, styleL);
      setCellStyle (sheet, i, lastCol, styleR);
    }

    for (int i = firstCol + 3; i < lastCol; i += 3)
      setCellStyle (sheet, firstRow + 1, i, styleDuration);

    for (int i = firstRow; i <= lastRow; ++i)
      sheet.getRow (i).setHeight ((short)280);

    sheet.groupRow (firstRow, lastRow);
  }


  protected void setCellStyle (HSSFSheet sheet, int rowNum, int col,
                               HSSFCellStyle style)
  {
    HSSFRow row = sheet.getRow (rowNum);
    if (null == row)
      row = sheet.createRow (rowNum);

    HSSFCell cell = row.getCell (col);
    if (null == cell)
      cell = row.createCell (col);

    cell.setCellStyle (style);
  }


  protected HSSFCellStyle createBorder (HSSFWorkbook wb,
                                        boolean top,
                                        boolean left,
                                        boolean bottom,
                                        boolean right)
  {
    final HSSFCellStyle style = wb.createCellStyle();
    if (top) style.setBorderTop (CellStyle.BORDER_THICK);
    if (left) style.setBorderLeft (CellStyle.BORDER_THICK);
    if (bottom) style.setBorderBottom (CellStyle.BORDER_THICK);
    if (right) style.setBorderRight (CellStyle.BORDER_THICK);
    return style;
  }


  @Override
  public void write (OutputStream os) throws Exception
  {
    createWorkbook().write (os);
  }
}

