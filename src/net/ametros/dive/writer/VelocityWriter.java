/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.writer;

import net.ametros.dive.ComputedStop;
import net.ametros.dive.data.Gas;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;


/** Writes dive tables via Apache Velocity. */
public class VelocityWriter extends AbstractWriter
{
  private final VelocityEngine ve;
  private final String templateName;


  public VelocityWriter (String templateName) throws Exception
  {
    this.templateName = templateName;

    ve = new VelocityEngine();
    ve.setProperty (VelocityEngine.RESOURCE_LOADER, "classpath");
    ve.setProperty ("classpath.resource.loader.class",
                    ClasspathResourceLoader.class.getName());
    ve.init();

    if (!ve.resourceExists (templateName.toString()))
      throw new IllegalArgumentException ("No such template: " + templateName);
  }


  protected VelocityContext createContext() throws Exception
  {
    final VelocityContext vc = new VelocityContext();
    vc.put ("Math", Math.class);
    vc.put ("this", this);
    vc.put ("dives", dives);
    vc.put ("depths", dives.keySet());

    final Map<Integer, SortedSet<ComputedStop>> stopKeys =
      new HashMap<Integer, SortedSet<ComputedStop>>();
    final Map<Integer, Set<Gas>> gases = new HashMap<Integer, Set<Gas>>();

    for (int depth: dives.keySet())
    {
      stopKeys.put (depth, getStopKeys (depth));
      gases.put (depth, getGases (depth));
    }

    vc.put ("stopKeys", stopKeys);
    vc.put ("gases", gases);

    return vc;
  }


  @Override
  public void write (OutputStream os) throws Exception
  {
    final VelocityContext vc = createContext();

    OutputStreamWriter osw = null;

    try
    {
      osw = new OutputStreamWriter (os);
      if (!ve.mergeTemplate (templateName, "UTF-8", vc, osw))
        throw new Exception ("Failed to merge Velociy template");
    }
    finally
    {
      if (null != osw)
        osw.close();
    }
  }
}

