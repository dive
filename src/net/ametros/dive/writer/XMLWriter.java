/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.writer;

import net.ametros.dive.ComputedDive;
import net.ametros.dive.data.Dives;
import net.ametros.dive.data.ObjectFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;


/** Writes XML dive tables. */
public class XMLWriter implements Writer
{
  private static final SchemaFactory sf =
    SchemaFactory.newInstance (XMLConstants.W3C_XML_SCHEMA_NS_URI);

  private final Schema schema;
  private final JAXBContext jc;
  private final Dives dives;


  public XMLWriter() throws Exception
  {
    schema = sf.newSchema (Dives.class.getResource ("/dives.xsd"));
    jc = JAXBContext.newInstance ("net.ametros.dive.data");
    dives = new ObjectFactory().createDives();
  }


  @Override
  public void clear()
  {
    dives.getDives().clear();
  }


  @Override
  public void add (Iterable<ComputedDive> dives)
  {
    for (ComputedDive d: dives)
      add (d);
  }


  @Override
  public void add (ComputedDive[] dives)
  {
    for (ComputedDive d: dives)
      add (d);
  }


  @Override
  public void add (ComputedDive dive)
  {
    dives.getDives().add (dive.getDive());
  }


  @Override
  public void write (File file) throws Exception
  {
    OutputStream os = null;
    boolean deleteFile = false;

    try
    {
      os = new FileOutputStream (file);
      deleteFile = true;
      write (os);
      os.close();
      os = null;
      deleteFile = false;
    }
    finally
    {
      try { if (null != os) os.close(); }
      finally { if (deleteFile) file.delete(); }
    }
  }


  @Override
  public void write (OutputStream os) throws Exception
  {
    final Marshaller m = jc.createMarshaller();
    m.setSchema (schema);
    m.setProperty (Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    m.marshal (dives, os);
  }
}

