/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.writer;

import java.io.File;


/** Factory for dive plan writers. */
public class WriterFactory
{
  public Writer getWriter (File file) throws Exception
  {
    final String name = file.getName();
    final int dot = name.lastIndexOf ('.');
    if (-1 == dot)
      throw new IllegalArgumentException ("Cannot guess output format");

    return getWriter (name.substring (dot + 1));
  }


  public Writer getWriter (String format) throws Exception
  {
    format = format.toLowerCase();

    if ("html".equals (format) || "xhtml".equals (format))
      return new VelocityWriter ("/" + format + ".vm");

    if ("xls".equals (format))
      return new XLSWriter();

    if ("xml".equals (format))
      return new XMLWriter();

    throw new IllegalArgumentException ("No known writer for " + format);
  }
}

