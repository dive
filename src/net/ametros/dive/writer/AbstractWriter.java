/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.writer;

import net.ametros.dive.ComputedDive;
import net.ametros.dive.ComputedStop;
import net.ametros.dive.data.Gas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Writes dive tables. */
public abstract class AbstractWriter implements Writer
{
  /** Groups dives by depth, and then by duration. */
  protected final Map<Integer, SortedSet<ComputedDive>> dives =
    new TreeMap<Integer, SortedSet<ComputedDive>>();


  @Override
  public void clear()
  {
    dives.clear();
  }


  @Override
  public void add (Iterable<ComputedDive> dives)
  {
    for (ComputedDive d: dives)
      add (d);
  }


  @Override
  public void add (ComputedDive[] dives)
  {
    for (ComputedDive d: dives)
      add (d);
  }


  @Override
  public void add (ComputedDive dive)
  {
    final int depth = dive.getDepth();

    if (!dives.containsKey (depth))
    {
      dives.put (depth,
                 new TreeSet<ComputedDive> (new Comparator<ComputedDive>() {
                   @Override
                   public int compare (ComputedDive d1, ComputedDive d2)
                   {
                     return d1.getDuration() - d2.getDuration();
                   }
                 }));
    }

    dives.get (depth).add (dive);
  }


  @Override
  public void write (File file) throws Exception
  {
    OutputStream os = null;
    boolean deleteFile = false;

    try
    {
      os = new FileOutputStream (file);
      deleteFile = true;
      write (os);
      os.close();
      os = null;
      deleteFile = false;
    }
    finally
    {
      try { if (null != os) os.close(); }
      finally { if (deleteFile) file.delete(); }
    }
  }


  private static final Pattern formatNx = Pattern.compile ("NX_(\\d+)");
  private static final Pattern formatTx = Pattern.compile ("TX_(\\d+)_(\\d+)");

  public String format (Gas gas)
  {
    final String str = "" + gas;

    Matcher m = formatNx.matcher (str);
    if (m.matches())
      return "Nx " + m.group (1);

    m = formatTx.matcher (str);
    if (m.matches())
      return "Tx " + m.group (1) + '/' + m.group (2);

    return str;
  }


  /** Gets a set of stops representing all (Gas, Depth) pairs for all stops
   *  for all dives to a given bottom depth, ordered by increasing depth and
   *  then by gas.
   */
  protected SortedSet<ComputedStop> getStopKeys (int bottomDepth)
  {
    final SortedSet<ComputedStop> stops =
      new TreeSet<ComputedStop> (new Comparator<ComputedStop>() {
        @Override
        public int compare (ComputedStop s1, ComputedStop s2)
        {
          final int d1 = s1.getDepth();
          final int d2 = s2.getDepth();

          if (d1 != d2)
            return d1 - d2;

          final Gas g1 = s1.getGas();
          final Gas g2 = s2.getGas();

          return g1.toString().compareTo (g2.toString());
        }
      });

    for (ComputedDive d: dives.get (bottomDepth))
      for (ComputedStop s: d.getStops())
        stops.add (s);

    return stops;
  }


  /** Gets a set of the gases used for dives to a given bottom depth. */
  protected Set<Gas> getGases (int bottomDepth)
  {
    final Set<Gas> gases = EnumSet.noneOf (Gas.class);

    for (ComputedDive d: dives.get (bottomDepth))
    {
      gases.add (d.getGas());

      for (ComputedStop s: d.getStops())
        gases.add (s.getGas());
    }

    return gases;
  }


  public long ceil (double in)
  {
    return (long)Math.ceil (in);
  }
}

