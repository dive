/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.parser;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;
import net.ametros.dive.data.Stop;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;


/** Parser for APD Projection 2.x HTML dive plans. */
public class APDProjectionParser extends AbstractParser
{
  @Override
  protected void doParse (File file) throws IOException
  {
    FileReader fr = null;

    try
    {
      fr = new FileReader (file);
      new ParserDelegator().parse (fr, new ParserCallback(), true);
    }
    finally
    {
      if (null != fr)
        fr.close();
    }
  }


  enum ParserState
  {
    INITIAL,      // Look for text = "Segment Time" then FIND_ROW. We'd look
                  // for <TBODY> but Swing's HTML parser ignores it.
    FIND_ROW,     // Look for <TR> then BUILD_ROW.
    BUILD_ROW,    // Append text components to list; on </TR> process the row
                  // then FIND_ROW.
  }


  class ParserCallback extends HTMLEditorKit.ParserCallback
  {
    private ParserState state;
    private Dive dive;
    private boolean nextIsBottom;
    private boolean ascentComplete;
    private final List<String> tokens = new ArrayList<String>();


    public ParserCallback()
    {
      reset();
    }


    public void reset()
    {
      state = ParserState.INITIAL;
      tokens.clear();
      nextIsBottom = false;
      ascentComplete = false;

      dive = of.createDive();
      dive.setAscent (of.createAscent());
      dive.setDuration (0);
      dive.setPhysiology (createPhysiology());
      dive.setWater (createWater());
    }


    private void handleTokens()
    {
      if (5 != tokens.size())
        return;

      if ("Bottom Depth".equals (tokens.get (2)))
      {
        nextIsBottom = true;
        return;
      }

      if (nextIsBottom)
      {
        dive.setDepth (Integer.parseInt (tokens.get (2)));
        dive.setGas (parseGas (tokens.get (0)));
        dive.setDuration (Integer.parseInt (tokens.get (3)));
        ascentComplete = true;
        nextIsBottom = false;
        return;
      }

      if (ascentComplete)
      {
        dive.setDuration (dive.getDuration() +
                          Integer.parseInt (tokens.get (3)));
        return;
      }

      final Stop stop = of.createStop();
      stop.setGas (parseGas (tokens.get (0)));
      stop.setDepth (Integer.parseInt (tokens.get (2)));
      stop.setDuration (Integer.parseInt (tokens.get (3)));

      // APD tends to output multiple stops at the same depth. Our output
      // stages don't handle that, so coalesce such stops into a single stop.
      final List<Stop> stops = dive.getAscent().getStops();
      final int numStops = stops.size();

      if (0 < stops.size())
      {
        final Stop last = stops.get (numStops - 1);
        if (last.getDepth() == stop.getDepth())
        {
          if (!last.getGas().equals (stop.getGas()))
            throw new IllegalArgumentException ("Multiple stop gases at " +
                                                stop.getDepth() + "m");

          last.setDuration (last.getDuration() + stop.getDuration());
          return;
        }
      }

      stops.add (stop);
    }


    @Override
    public void handleEndTag (HTML.Tag tag, int pos)
    {
      // System.err.println ("End " + tag);

      switch (state)
      {
        case BUILD_ROW:
          if (HTML.Tag.TR.equals (tag))
          {
            handleTokens();
            tokens.clear();
            state = ParserState.FIND_ROW;
          }

          break;

        case FIND_ROW:
          if (HTML.Tag.HTML.equals (tag) &&
              ascentComplete &&
              (0 < dive.getDuration()))
          {
            addDive (dive);
          }

        default:
          break;
      }
    }


    @Override
    public void handleStartTag (HTML.Tag tag, MutableAttributeSet a, int pos)
    {
      // System.err.println ("Start " + tag);

      switch (state)
      {
        case FIND_ROW:
          if (HTML.Tag.TR.equals (tag))
            state = ParserState.BUILD_ROW;
          break;

        default:
          break;
      }
    }


    @Override
    public void handleText (char[] data, int pos)
    {
      switch (state)
      {
        case INITIAL:
          if ("Segment Time".equals (normaliseWhitespace (data)))
            state = ParserState.FIND_ROW;

          break;

        case BUILD_ROW:
          tokens.add (normaliseWhitespace (data));
          break;

        default:
          break;
      }
    }
  }
}

