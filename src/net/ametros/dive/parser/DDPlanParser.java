/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.parser;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;
import net.ametros.dive.data.Stop;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;


/** Parser for DDPlan 2.x HTML dive plans. */
public class DDPlanParser extends AbstractParser
{
  private static final Pattern depthPattern = Pattern.compile ("^(\\d+)m$");

  private static final Pattern stopPattern =
    Pattern.compile ("^(\\d+)\\s*/\\s*\\d+$");


  @Override
  protected void doParse (File file) throws IOException
  {
    FileReader fr = null;

    try
    {
      fr = new FileReader (file);
      new ParserDelegator().parse (fr, new ParserCallback(), true);
    }
    finally
    {
      if (null != fr)
        fr.close();
    }
  }


  enum ParserState
  {
    INITIAL,      // Look for <table>.
    FIND_DEPTH,   // First text gives dive depth, then START_DIVE.
    START_DIVE,   // Look for <table>.
    TITLES,       // Texts are column names, then look for </tr>. Special
                  // column names are Gas, ppO2, D\RT; others are durations.
    STOPS,        // Texts are column values, then look for Text CNS%.
    GRADIENTS;    // Text starting "Gradient Factors", then INITIAL.
  }


  class ParserCallback extends HTMLEditorKit.ParserCallback
  {
    private ParserState state;
    private int diveDepth;
    private final List<String> columns = new ArrayList<String>();
    private Dive[] dives;
    private int nextCol;
    private Gas stopGas;
    private int stopDepth;


    public ParserCallback()
    {
      reset();
    }


    public void reset()
    {
      state = ParserState.INITIAL;
      diveDepth = 0;
      columns.clear();
      dives = null;
      nextCol = 0;
      stopGas = null;
      stopDepth = 0;
    }


    @Override
    public void handleEndTag (HTML.Tag tag, int pos)
    {
      // System.err.println ("End " + tag);

      switch (state)
      {
        case TITLES:
          if (HTML.Tag.TR.equals (tag))
          {
            dives = new Dive[columns.size()];
            int i = 0;

            for (String str: columns)
            {
              if (!"Gas".equals (str) &&
                  !"ppO2".equals (str) &&
                  !"D\\RT".equals (str))
              {
                final int duration = Integer.parseInt (str);
                dives[i] = createDive (diveDepth, duration, getDefaultGas());
              }

              ++i;
            }

            state = ParserState.STOPS;
          }

          break;

        default:
          break;
      }
    }


    @Override
    public void handleStartTag (HTML.Tag tag, MutableAttributeSet a, int pos)
    {
      // System.err.println ("Start " + tag);

      switch (state)
      {
        case INITIAL:
          if (HTML.Tag.TABLE.equals (tag))
            state = ParserState.FIND_DEPTH;
          break;

        case START_DIVE:
          if (HTML.Tag.TABLE.equals (tag))
            state = ParserState.TITLES;
          break;

        default:
          break;
      }
    }


    @Override
    public void handleText (char[] data, int pos)
    {
      final String text = normaliseWhitespace (data);
      // System.err.println ("Text: " + text);

      switch (state)
      {
        case FIND_DEPTH:
          final Matcher m0 = depthPattern.matcher (text);
          if (!m0.matches())
            throw new RuntimeException ("Expected dive depth; got " + text);

          diveDepth = Integer.parseInt (m0.group (1));
          state = ParserState.START_DIVE;
          break;

        case TITLES:
          columns.add (text);
          break;

        case STOPS:
          if (text.startsWith ("CNS%"))
            state = ParserState.GRADIENTS;
          else
          {
            if (nextCol == columns.size())
            {
              stopGas = null;
              stopDepth = 0;
              nextCol = 0;
            }

            final String key = columns.get (nextCol);
            if ("Gas".equals (key))
            {
              stopGas = parseGas (text);
            }

            else if ("D\\RT".equals (key))
            {
              stopDepth = Integer.parseInt (text);
            }

            else if (!"ppO2".equals (key) && !text.isEmpty())
            {
              final Matcher m1 = stopPattern.matcher (text);
              if (!m1.matches())
                throw new RuntimeException ("Expected stop; got " + text);

              final Stop stop = of.createStop();
              stop.setDepth (stopDepth);
              stop.setDuration (Integer.parseInt (m1.group (1)));
              stop.setGas (stopGas);
              dives[nextCol].getAscent().getStops().add (stop);
            }

            ++nextCol;
          }

          break;

        case GRADIENTS:
          if (text.startsWith ("Gradient Factors"))
          {
            // TODO: WTF are gradient factors?

            for (Dive dive: dives)
              if (null != dive)
                addDive (dive);

            reset();
          }

          break;

        default:
          break;
      }
    }
  }
}

