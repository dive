/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.parser;

import java.io.File;
import java.io.FileReader;


/** Factory for dive plan parsers. */
public class ParserFactory
{
  public Parser getParser (File file) throws Exception
  {
    final String name = file.getName().toLowerCase();

    if (name.endsWith (".xls"))
      return new XLSParser();

    if (name.endsWith (".xml"))
      return new XMLParser();

    if (name.endsWith (".html") || name.endsWith (".html"))
    {
      final String str = readFile (file);

      if (str.contains ("<B>&nbsp; &nbsp; DDPLAN v"))
        return new DDPlanParser();

      if (str.contains ("<meta name=\"author\" content=\"APD Ltd.\">"))
        return new APDProjectionParser();
    }

    throw new IllegalArgumentException ("No known parser for " + file);
  }


  protected String readFile (File file) throws Exception
  {
    final long length = file.length();
    if ((int)length != length)
      throw new IllegalArgumentException ("File is too large");

    final char[] chs = new char[(int)length];

    FileReader fr = null;

    try
    {
      fr = new FileReader (file);
      final int chars = fr.read (chs);
      return new String (chs, 0, chars);
    }
    finally
    {
      if (null != fr)
        fr.close();
    }
  }
}

