/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.parser;

import net.ametros.dive.data.Ascent;
import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Dives;
import net.ametros.dive.data.Gas;
import net.ametros.dive.data.ObjectFactory;
import net.ametros.dive.data.Physiology;
import net.ametros.dive.data.Stop;
import net.ametros.dive.data.Water;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Parses dive plans. */
public abstract class AbstractParser implements Parser
{
  private static final Map<String, Gas> gasMap = new HashMap<String, Gas>();
  static
  {
    for (Gas gas: Gas.values())
    {
      String str = gas.toString().toLowerCase();
      gasMap.put (str, gas);
      gasMap.put (str.replace ('_', '/'), gas);
      gasMap.put (str.replace ('_', ' '), gas);
      gasMap.put (str.replace ('_', '-'), gas);
      gasMap.put (str.replace ("_", "--"), gas);

      if (str.startsWith ("tx_"))
      {
        str = str.substring (3);
        gasMap.put (str.replace ('_', '/'), gas);
        gasMap.put (str.replace ('_', ' '), gas);
        gasMap.put (str.replace ('_', '-'), gas);
        gasMap.put (str.replace ("_", "--"), gas);
      }
      else if (str.startsWith ("nx_"))
      {
        gasMap.put ("nx" + str.substring (3), gas);
        gasMap.put (str.substring (3) + "% nitrox", gas);
      }
    }
  }

  protected final ObjectFactory of = new ObjectFactory();

  private Dives dives;


  protected abstract void doParse (File file) throws Exception;


  @Override
  public Dives parse (File file) throws Exception
  {
    try
    {
      dives = of.createDives();
      doParse (file);
      return dives;
    }
    finally
    {
      dives = null;
    }
  }


  protected Gas parseGas (String in)
  {
    final String lower = in.toLowerCase();
    if (gasMap.containsKey (lower))
      return gasMap.get (lower);

    return Gas.valueOf (in);
  }


  protected Dive createDive (int depth, int duration, Gas gas)
  {
    final Dive dive = of.createDive();
    dive.setAscent (of.createAscent());
    dive.setDepth (depth);
    dive.setDuration (duration);
    dive.setGas (gas);
    dive.setPhysiology (createPhysiology());
    dive.setWater (createWater());
    return dive;
  }


  private int getDefault (String property, int value)
  {
    final String str = System.getProperty (property);
    return (null == str) ? value : Integer.parseInt (str);
  }


  protected Gas getDefaultGas()
  {
    final String str = System.getProperty ("ametros.dive.defaultGas");
    return (null == str) ? Gas.TX_18_45 : parseGas (str);
  }


  protected Water getDefaultWater()
  {
    final Water water = of.createWater();
    water.setDensity (getDefault ("ametros.dive.defaultDensity", 1025));
    return water;
  }


  protected Water createWater()
  {
    return getDefaultWater();
  }


  protected Physiology getDefaultPhysiology()
  {
    final Physiology phys = of.createPhysiology();
    phys.setSacr (getDefault ("ametros.dive.defaultSacr", 20));
    phys.setSaca (getDefault ("ametros.dive.defaultSaca", 30));
    return phys;
  }


  protected Physiology createPhysiology()
  {
    return getDefaultPhysiology();
  }


  protected void addDives (Dive[] dives)
  {
    if (null != dives)
      for (Dive dive: dives)
        addDive (dive);
  }


  protected void addDives (Iterable<Dive> dives)
  {
    if (null != dives)
      for (Dive dive: dives)
        addDive (dive);
  }


  protected void addDive (Dive dive)
  {
    dives.getDives().add (dive);
  }


  /** Replaces non-breaking spaces with ' '; trims leading/trailing
   *  whitespace.
   */
  protected String normaliseWhitespace (char[] in)
  {
    final char[] out = new char[in.length];

    for (int i = 0; i < in.length; ++i)
      out[i] = ('\u00a0' == in[i]) ? ' ' :  in[i];

    return new String (out).trim();
  }
}

