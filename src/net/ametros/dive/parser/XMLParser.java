/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.parser;

import net.ametros.dive.data.Dives;

import java.io.File;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;


/** Parser for ADC XML dive plans. */
public class XMLParser implements Parser
{
  private static final SchemaFactory sf =
    SchemaFactory.newInstance (XMLConstants.W3C_XML_SCHEMA_NS_URI);

  private final Schema schema;
  private final JAXBContext jc;


  public XMLParser() throws Exception
  {
    schema = sf.newSchema (Dives.class.getResource ("/dives.xsd"));
    jc = JAXBContext.newInstance ("net.ametros.dive.data");
  }


  public Dives parse (File file) throws Exception
  {
    final Unmarshaller u = jc.createUnmarshaller();
    u.setSchema (schema);

    return (Dives)u.unmarshal (file);
  }
}

