/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive.parser;

import net.ametros.dive.data.Ascent;
import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;
import net.ametros.dive.data.ObjectFactory;
import net.ametros.dive.data.Physiology;
import net.ametros.dive.data.Stop;
import net.ametros.dive.data.Water;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;


/** Parser for Tickle Excel dive plans. */
public class XLSParser extends AbstractParser
{
  // \s doesn't include 160 (non-breaking space).
  private static final Pattern depthPattern =
    Pattern.compile ("^(?:\u00a0|\\s)*(\\d+)m(?:\u00a0|\\s)*$");

  // \s doesn't include 160 (non-breaking space).
  private static final Pattern numberPattern =
    Pattern.compile ("^(?:\u00a0|\\s)*(\\d+)(?:\u00a0|\\s)*$");

  // \s doesn't include 160 (non-breaking space).
  private static final Pattern stopPattern =
    Pattern.compile ("^(?:\u00a0|\\s)*(\\d+)-+(\\d+)(?:\u00a0|\\s)*$");


  @Override
  protected void doParse (File file) throws Exception
  {
    InputStream is = null;
    HSSFWorkbook wb = null;

    try
    {
      is = new FileInputStream (file);
      wb = new HSSFWorkbook (is);
    }
    finally
    {
      if (null != is)
        try { is.close(); } catch (Exception e) { e.printStackTrace(); }
    }

    parse (wb.getSheetAt (0));
  }


  protected int parseInt (HSSFRow row, int cell)
  {
    if (null == row)
      throw new IllegalArgumentException ("Expected number");

    return parseInt (row.getCell (cell));
  }


  protected int parseInt (HSSFCell cell)
  {
    if (null == cell)
      throw new IllegalArgumentException ("Expected number");

    switch (cell.getCellType())
    {
      case Cell.CELL_TYPE_STRING:
        final String str = cell.getStringCellValue();
        final Matcher m = numberPattern.matcher (str);
        if (!m.matches())
          throw new IllegalArgumentException ("Expected number at " + str);

        return Integer.parseInt (m.group (1));

      case Cell.CELL_TYPE_NUMERIC:
        return (int)cell.getNumericCellValue();

      case Cell.CELL_TYPE_BLANK:
        throw new IllegalArgumentException ("Expected number in blank cell");

      case Cell.CELL_TYPE_FORMULA:
        throw new IllegalArgumentException ("Unsupported cell type: formula");

      case Cell.CELL_TYPE_BOOLEAN:
        throw new IllegalArgumentException ("Unsupported cell type: boolean");

      case Cell.CELL_TYPE_ERROR:
        throw new IllegalArgumentException ("Unsupported cell type: error");

      default:
        throw new IllegalArgumentException ("Unsupported cell type: " +
                                            cell.getCellType());
    }
  }


  protected void parse (HSSFSheet sheet)
  {
    final int rows = sheet.getPhysicalNumberOfRows();
    Dive[] dives = null;

    for (int rowNum = 0; rowNum < rows; ++rowNum)
    {
      HSSFRow row = sheet.getRow (rowNum);
      if (null == row)
        continue;

      // Look for a row with depthPattern in its first cell.
      final HSSFCell cell = row.getCell (0);
      if (null == cell)
        continue;

      final String value = cell.getStringCellValue();
      final Matcher m0 = depthPattern.matcher (value);
      if (m0.matches())
      {
        final int depth = Integer.parseInt (m0.group (1));

        // Next row must have "Gas" in its first cell.
        row = sheet.getRow (++rowNum);
        if (null == cell)
          throw new IllegalArgumentException (rowNum + ": Expected Gas");

        final HSSFCell gas = row.getCell (0);
        final HSSFCell drt = row.getCell (1);
        if ((null == gas) || !"Gas".equals (gas.getStringCellValue()))
          throw new IllegalArgumentException (rowNum + ": Expected Gas");
        if ((null == drt) || !"D\\RT".equals (drt.getStringCellValue()))
          throw new IllegalArgumentException (rowNum + ": Expected D\\RT");

        // Construct Dive objects.
        addDives (dives);
        dives = new Dive[row.getPhysicalNumberOfCells() - 2];

        for (int i = 0; i < dives.length; ++i)
          dives[i] = createDive (depth, parseInt (row, i + 2), getDefaultGas());

        continue;
      }

      if (!"".equals (value))
      {
        // Here => adding stops.
        if (null == dives)
          throw new IllegalArgumentException ("No dive depth found");

        final Gas gas = parseGas (value);
        final int depth = parseInt (row, 1);

        for (int i = 0; i < dives.length; ++i)
        {
          final HSSFCell stopCell = row.getCell (i + 2);
          if (null == stopCell)
            continue;

          final Matcher m1 =
            stopPattern.matcher (stopCell.getStringCellValue());
          if (m1.matches())
          {
            final int duration = Integer.parseInt (m1.group (1));

            final Stop stop = of.createStop();
            stop.setDepth (depth);
            stop.setDuration (duration);
            stop.setGas (gas);
            dives[i].getAscent().getStops().add (stop);
          }
        }
      }
    }

    addDives (dives);
  }
}

