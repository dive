/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Dives;
import net.ametros.dive.parser.Parser;
import net.ametros.dive.parser.ParserFactory;
import net.ametros.dive.writer.Writer;
import net.ametros.dive.writer.WriterFactory;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import java.io.File;
import java.util.List;
import java.util.ArrayList;


public class Main
{
  private static List<String> L (String... strs)
  {
    final List<String> l = new ArrayList<String> (strs.length);
    for (String str: strs)
      l.add (str);

    return l;
  }


  private static void usage (String err, OptionParser op)
  {
    try
    {
      if (null != err)
      {
        System.err.println (err);
        System.err.println();
      }

      if (null != op)
      {
        System.err.println ("Usage: java -jar dive.jar [options] files...\n");
        op.printHelpOn (System.err);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }


  public static void main (String[] args) throws Exception
  {
    System.err.println (
      "Ametros Dive Computer  Copyright (C) 2010 Geoff Johnstone\n" +
      "This program comes with ABSOLUTELY NO WARRANTY.\n" +
      "This is free software, and you are welcome to redistribute it\n" +
      "under certain conditions. Please see the file COPYING that you\n" +
      "should have received with this software.\n"
    );

    final OptionParser op = new OptionParser();
    final OptionSpec<String> format =
      op.acceptsAll (L("f", "format"), "Output format").
        withRequiredArg().ofType (String.class);
    final OptionSpec<File> output =
      op.acceptsAll (L("o", "output"), "Output filename").
        withRequiredArg().ofType (File.class);
    final OptionSpec<Double> ascent =
      op.acceptsAll (L("a", "ascent"), "Ascent speed, m/s").
        withRequiredArg().ofType (Double.class).
        defaultsTo (8.0);
    final OptionSpec<Integer> density =
      op.acceptsAll (L("d", "density"), "Default water density, kg/m\u00B3").
        withRequiredArg().ofType (Integer.class).
        defaultsTo (1025);
    final OptionSpec<Integer> sacr =
      op.accepts ("sacr", "Default SAC (resting), l/min").
        withRequiredArg().ofType (Integer.class).
        defaultsTo (20);
    final OptionSpec<Integer> saca =
      op.accepts ("saca", "Default SAC (active), l/min").
        withRequiredArg().ofType (Integer.class).
        defaultsTo (30);
    final OptionSpec<String> gas =
      op.acceptsAll (L("g", "gas"), "Default bottom gas").
        withRequiredArg().ofType (String.class).
        defaultsTo ("18/45");

    final WriterFactory wf = new WriterFactory();
    OptionSet options = null;
    Writer writer = null;

    try
    {
      options = op.parse (args);

      if (options.has (format))
        writer = wf.getWriter (options.valueOf (format));
      else if (!options.has (output))
        throw new IllegalArgumentException ("Cannot guess output format " +
                                            "without an output filename");
      else
        writer = wf.getWriter (options.valueOf (output));
    }
    catch (Exception e)
    {
      usage (e.getMessage(), op);
      System.exit (1);
    }

    final List<String> inputs = options.nonOptionArguments();
    if (0 == inputs.size())
    {
      usage ("No input files.", op);
      System.exit (2);
    }

    final double V_a = options.valueOf (ascent);
    System.setProperty ("ametros.dive.defaultDensity",
                        options.valueOf (density).toString());
    System.setProperty ("ametros.dive.defaultSacr",
                        options.valueOf (sacr).toString());
    System.setProperty ("ametros.dive.defaultSaca",
                        options.valueOf (saca).toString());
    System.setProperty ("ametros.dive.defaultGas",
                        options.valueOf (gas).toString());

    final ParserFactory pf = new ParserFactory();
    final DiveComputer dc = new DiveComputer();
    boolean gotPlans = false;

    for (String str: inputs)
    {
      final File file = new File (str);
      final Parser parser = pf.getParser (file);

      System.err.println ("Parsing dive plan(s) from " + file + "...");
      final Dives dives = parser.parse (file);
      System.err.println ("Parsed " + dives.getDives().size() +
                          " dive plan(s).");

      for (Dive dive: dives.getDives())
      {
        writer.add (dc.compute (dive, V_a));
        gotPlans = true;
      }
    }

    if (!gotPlans)
    {
      System.err.println ("No dive plans to output.");
      System.exit (3);
    }

    if (options.has (output))
    {
      final File file = options.valueOf (output);
      writer.write (file);
      System.err.println ("Written plan(s) to " + file.getAbsolutePath() + ".");
    }
    else
    {
      writer.write (System.out);
      System.err.println ("Written plan(s) to standard output.");
    }

    System.err.println ("\n*** You use these plans at your own risk. ***\n");
    System.exit (0);
  }
}

