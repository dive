/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;
import net.ametros.dive.data.Stop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/** A simple dive computer. */
public class DiveComputer
{
  private static final Comparator<Stop> cmp = new Comparator<Stop>() {
    @Override
    public int compare (Stop s1, Stop s2)
    {
      return s2.getDepth() - s1.getDepth();   // Reverse order of depth.
    }
  };

  private static final double g = 9.80665;
  private static final double atm = 101325.0;

  private final SafetyMargin safetyMargin;

  private Dive dive;

  private double P_m;


  /** Creates a new DiveComputer using the DefaultSafetyMargin. */
  public DiveComputer()
  {
    this (new DefaultSafetyMargin());
  }


  /** Creates a new DiveComputer.
   *  @param safetyMargin the safety margin to apply to calculations.
   */
  public DiveComputer (SafetyMargin safetyMargin)
  {
    this.safetyMargin = safetyMargin;
  }


  /** Computes the gas volumes required for a dive.
   *  @param dive the dive.
   *  @param V_a constant rate of ascent, metres per second.
   *  @return a computed dive representing the given dive.
   */
  public synchronized ComputedDive compute (Dive dive, double V_a)
  {
    try
    {
      setDive (dive);
      return new ComputedDive (dive, V_a, this);
    }
    finally
    {
      setDive (null);
    }
  }


  /** P_m = g \rho \over 101325 */
  protected final double getP_m()
  {
    return P_m;
  }


  protected final void setDive (Dive dive)
  {
    this.dive = dive;
    this.P_m = (null == dive) ? 0 : (g * dive.getWater().getDensity() / atm);
  }


  protected final Dive getDive()
  {
    return dive;
  }


  /** Gets the volume of gas used for a decompression stop. <b>No safety
   *  margin is applied during this calculation.</b>
   *  @param stop the decompression stop.
   *  @return the number of litres of gas required for the stop.
   */
  public double getStopGasVolume (Stop stop)
  {
    final Dive dive = getDive();

    final double d = stop.getDepth();
    final double P_d = 1 + d * P_m;
    final double RMV_r = P_d * dive.getPhysiology().getSacr();

    return RMV_r * stop.getDuration();
  }


  /** Gets the volume of a given gas used for all decompression stop.
   *  @param gas the gas.
   *  @return the number of litres of gas required for the decompression stops.
   */
  public double getStopPhaseVolume (Gas gas)
  {
    final Dive dive = getDive();

    double litres = 0;
    for (Stop stop: dive.getAscent().getStops())
    {
      if (gas.equals (stop.getGas()))
      {
        litres += getStopGasVolume (stop);
      }
    }

    return safetyMargin.getStopPhaseVolume (dive, gas, litres);
  }


  /** Gets the volume of a given gas used during the ascent, excluding stops.
   *  @param V_a constant rate of ascent, metres per second.
   *  @param gas the gas.
   *  @return the number of litres of the given gas required for the ascent.
   */
  public double getAscentPhaseVolume (double V_a, Gas gas)
  {
    final Dive dive = getDive();

    // Find the (depth) interval during which the given gas is used, which,
    // with V_a, gives us t_0 = time at the deep end of the interval and
    // t_1 = time at the shallow end of the interval.
    //
    // We then compute
    // G_A' = SAC_a (t_1 - t_0) (1 + dP_m - ((t_1 + t_0) V_a P_m) / 2)
    double litres;
    final Interval i = getInterval (gas);
    // System.err.println ("" + gas + " ascent interval: " + i + " (" +
    //                     i.getRange() + "m)");

    if ((null == i) || (0 == i.getRange()))
      litres = 0;
    else
    {
      final double d = dive.getDepth();
      final double t_0 = (d - i.getHigher()) / V_a;
      final double t_1 = (d - i.getLower()) / V_a;

      litres = (double)dive.getPhysiology().getSaca() *
               (t_1 - t_0) * (1.0 + d * P_m - ((t_1 + t_0) * V_a * P_m) / 2.0);
    }

    return safetyMargin.getAscentPhaseVolume (dive, gas, litres);
  }


  /** Gets the depth interval within which a given gas is used whilst
   *  ascending from a dive (excluding stops).
   *  @param gas the gas.
   *  @return the depth interval for which the given gas is used.
   */
  public Interval getInterval (Gas gas)
  {
    final Dive dive = getDive();

    // Construct a list of stops, sorted in descending order of depth. We add
    // a synthetic Stop of duration 0 at the dive depth and another at the
    // surface to make gas interval calculations easier.
    final List<Stop> stops = new ArrayList<Stop> (dive.getAscent().getStops());

    final Stop bottom = new Stop();
    bottom.setDepth (dive.getDepth());
    bottom.setDuration (0);
    bottom.setGas (dive.getGas());
    stops.add (bottom);

    Collections.sort (stops, cmp);

    final Stop top = new Stop();
    top.setDepth (0);
    top.setDuration (0);
    top.setGas (stops.get (stops.size() - 1).getGas());
    stops.add (top);

    final int numStops = stops.size();

    // Now iterate through the collection to find the first (deepest) stop
    // with the given gas. Once we've found that, keep going until the gas
    // changes or we reach the surface. If we're not then at the surface then
    // continue to ensure that the gas isn't used again during the ascent,
    // which we don't handle.
    Stop deepest = null;
    Stop shallowest = null;

    for (Stop stop: stops)
    {
      final Gas g = stop.getGas();
      // System.err.println ("Stop: " + stop.getDepth() + "m on " + g);

      if (null == deepest)
      {
        if (gas.equals (g))
          deepest = stop;
      }

      else if (null == shallowest)
      {
        if (!gas.equals (g))
          shallowest = stop;
      }

      else if (gas.equals (g))
        throw new IllegalStateException ("" + gas + " is used more than once");
    }

    if (null == deepest)
      return null;

    if (null == shallowest)
    {
      if (!top.getGas().equals (deepest.getGas()))
        throw new AssertionError();

      shallowest = top;
    }

    final Interval i = new Interval (deepest.getDepth(), shallowest.getDepth());
    return safetyMargin.getInterval (dive, gas, i);
  }


  /** Gets the volume of a given gas used during the ascent, including stops.
   *  @param V_a constant rate of ascent, metres per second.
   *  @param gas the gas.
   *  @return the number of litres of the given gas required for the ascent.
   */
  public double getAscentTotalVolume (double V_a, Gas gas)
  {
    final double G_S = getStopPhaseVolume (gas);
    final double G_A = getAscentPhaseVolume (V_a, gas);

    // System.err.println ("" + gas + " stop litres: " + G_S);
    // System.err.println ("" + gas + " ascent litres: " + G_A);

    return safetyMargin.getAscentTotalVolume (getDive(), gas, G_S + G_A);
  }


  /** Gets the volume of gas required at the bottom of the dive.
   *  @return the number of litres of the gas required at the bottom.
   */
  public double getBottomVolume()
  {
    final Dive dive = getDive();

    final double d = dive.getDepth();
    final double P_d = 1 + d * P_m;
    final double RMV_a = P_d * dive.getPhysiology().getSaca();

    final double litres = RMV_a * dive.getDuration();
    return safetyMargin.getBottomVolume (dive, litres);
  }
}

