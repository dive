/* Ametros Dive Computer
 * Copyright (C) 2010 Geoff Johnstone
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.ametros.dive;

import net.ametros.dive.data.Dive;
import net.ametros.dive.data.Gas;


/** Default SafetyMargin implementation.
 *
 *  This assumes that Tx_18_45, normally used up to 12m, is used to the
 *  surface, and increases the required total ascent volumes of all other
 *  gases used during the ascent by 33%. Bottom gas volume is also increased
 *  by 33%.
 */
public class DefaultSafetyMargin implements SafetyMargin
{
  @Override
  public double getStopPhaseVolume (Dive dive, Gas gas, double original)
  {
    return original;
  }


  @Override
  public double getAscentPhaseVolume (Dive dive, Gas gas, double original)
  {
    return original;
  }


  @Override
  public double getAscentTotalVolume (Dive dive, Gas gas, double original)
  {
    // Add a third if not the bottom gas.
    if (!dive.getGas().equals (gas))
      return original * 4.0/3.0;

    return original;
  }


  @Override
  public double getBottomVolume (Dive dive, double original)
  {
    return original * 4.0/3.0;
  }


  @Override
  public Interval getInterval (Dive dive, Gas gas, Interval original)
  {
    // Use the bottom gas all the way to the surface.
    if (dive.getGas().equals (gas))
      return new Interval (original.getHigher(), 0);

    return original;
  }
}

